#include <common/ServiceDefinitions.h>
#include "JudpStructs2.h"


// Implement the Application Behavior for the Events Service
class MyEventsServiceImplementation : public EventsServiceDefinition
{
public:
    // Actions related to Application Behavior
    void createEvent() override
    {
        // Create Event and store internally
    }

    void updateEvent() override;
    void cancelEvent() override;
    void resetEventTimer() override;
    void stopEventTimer() override;

    void createCommandEvent() override;
    void updateCommandEvent() override;
    void startCommandTimer() override;
    void stopCommandTimer() override;

    // Guards /Conditions
    bool isSupported() override;
    bool eventExists() override;
    bool isCommandSupported() override;
};


// Implement the Application Behavior for The Transport Service
// This implementation only supports sending/receiving messages using JUDP
class MyJudpTransportServiceImplementation: public TransportServiceDefinition
{
public:
    MyJudpTransportServiceImplementation() :
        judp(0)
    {};

    void SetAddress(JausAddress address)
    {
        myAddress = address;
    }

    void Enqueue(Transport_SendRec sendRec)
    {
        judp2::SendOptions sendOptions;

        // JUDP doesn't specify any relationship between (JUDP) destination id and
        // send method (unicast v local broadcast v global broadcast), while JAUS
        // does have a relatonship between destination JAUS address and send method.
        // That relationship is enforced here.

        if (sendRec.DestinationID.SubsystemID == 65535)
        {
            // The use of GLOBAL_BROADCAST here is an example. The standard doesn't
            // require that Subsystem-level broacast messages be sent using JUDP global broadcast.
            sendOptions.SendMethod = judp2::SendOptions::GLOBAL_BROADCAST;
        }
        else if (sendRec.DestinationID.NodeID == 255)
        {
            // The use of LOCAL_BROADCAST here is an example. The standard doesn't
            // require that Node-level broacast messages be sent using JUDP local broadcast.
            sendOptions.SendMethod = judp2::SendOptions::LOCAL_BROADCAST;
        }
        else if (sendRec.DestinationID.ComponentID == 255)
        {
            // The use of LOCAL_BROADCAST here is an example. The standard doesn't
            // required that Component Broacast messages be sent using JUDP local broadcast.
            sendOptions.SendMethod = judp2::SendOptions::LOCAL_BROADCAST;
        }
        else
        {
            sendOptions.SendMethod = judp2::SendOptions::UNICAST;
        }

        // The client can decide to send messages with different priorities and/or with
        // different reliability requirements, based on the message being sent, by the
        // intended recipient, etc
        // It must still conform to the JUDP protocol however, so some combinations
        // may not be allowed (such as using BROADCAST and ACK/NAK together).
        if (sendRec.MessagePayload.Id == SetEmergency::ID)
        {
            sendOptions.Priority = judp2::PriorityEnum::PRIORITY_CRITICAL;


            if (sendOptions.SendMethod == judp2::SendOptions::UNICAST)
            {
                sendOptions.SendMethod = judp2::SendOptions::UNICAST_WITH_ACK;
            }
        }
        else if (sendRec.DestinationID.ComponentID == 23)
        {
            sendOptions.Priority = judp2::PriorityEnum::PRIORITY_LOW;
        }
        else
        {
            sendOptions.Priority = judp2::PriorityEnum::PRIORITY_NORMAL;
        }

        judp.Send(sendRec.MessagePayload.AsByteBuffer(), sendRec.DestinationID.AsUnit32(), sendOptions);
    }

private:
    JausAddress myAddress;
    judp2::JUDP judp;
};

// Put together the Service Implementations under a containing Component
class MyEventsServerComponent
{
public:
    MyEventsServerComponent()
    {
        eventsService.SetTransportServiceDefinition(&transportService);
    }

    void SetAddress(JausAddress address)
    {
        transportService.SetAddress(address);
    }

    int Run();

private:
    MyJudpTransportServiceImplementation transportService;
    MyEventsServiceImplementation eventsService;
};

int main()
{
    MyEventsServerComponent server;

    server.SetAddress(JausAddress("100.10.30"));
    return server.Run();
}
