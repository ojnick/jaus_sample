#include "JudpStructs2.h"
#include <common/JausStructs.h>

// The standard doesn't specify client behavior so the only requirement for a client
// is that is needs to send properly formed JAUS messages using a transport/communication
// protocol that the server understands. In this example, that is JUDP.
// As long as it conforms with the transport/communicationprotocol it is using the client
// can send messages with any options it likes. For JUDP that means a client can use any
// Priority, use Ack/Nak, or maybe even send JAUS unicast messages using JUDP Broadcast?


class MyClientComponent2
{
public:
    MyClientComponent2() :
        judp(0) // Using JUDP to send JAUS Messages
    {};

    void SetAddress(JausAddress address)
    {
        myAddress = address;
        judp.setSourceAddress(myAddress.AsUnit32());
    }

    void Send(JausAddress destination, JausMessage message)
    {
        judp2::SendOptions sendOptions;

        // JUDP doesn't specify any relationship between (JUDP) destination id and
        // send method (unicast v local broadcast v global broadcast), while JAUS
        // does have a relatonship between destination JAUS address and send method.
        // That relationship is enforced here.

        if (destination.SubsystemID == 65535)
        {
            // The use of GLOBAL_BROADCAST here is an example. The standard doesn't
            // require that Subsystem-level broacast messages be sent using JUDP global broadcast.
            sendOptions.SendMethod = judp2::SendOptions::GLOBAL_BROADCAST;
        }
        else if (destination.NodeID == 255)
        {
            // The use of LOCAL_BROADCAST here is an example. The standard doesn't
            // require that Node-level broacast messages be sent using JUDP local broadcast.
            sendOptions.SendMethod = judp2::SendOptions::LOCAL_BROADCAST;
        }
        else if (destination.ComponentID == 255)
        {
            // The use of LOCAL_BROADCAST here is an example. The standard doesn't
            // required that Component Broacast messages be sent using JUDP local broadcast.
            sendOptions.SendMethod = judp2::SendOptions::LOCAL_BROADCAST;
        }
        else
        {
            sendOptions.SendMethod = judp2::SendOptions::UNICAST;
        }

        // The client can decide to send messages with different priorities and/or with
        // different reliability requirements, based on the message being sent, by the
        // intended recipient, etc
        // It must still conform to the JUDP protocol however, so some combinations
        // may not be allowed (such as using BROADCAST and ACK/NAK together).
        if (message.Id == SetEmergency::ID)
        {
            sendOptions.Priority = judp2::PriorityEnum::PRIORITY_CRITICAL;

            // If not being sent using Broadcast, send the using ACK/NAK
            if (sendOptions.SendMethod == judp2::SendOptions::UNICAST)
            {
                sendOptions.SendMethod = judp2::SendOptions::UNICAST_WITH_ACK;
            }
        }
        else if (destination.ComponentID == 23)
        {
            sendOptions.Priority = judp2::PriorityEnum::PRIORITY_LOW;
        }
        else
        {
            sendOptions.Priority = judp2::PriorityEnum::PRIORITY_NORMAL;
        }

        // The JAUS address is translated to a JUDP/JTCP/JSerial address and the work
        // of actually sending is delegated to the JUDP class. At this level
        // the IP address & port is completely hidden.

        judp.Send(message.AsByteBuffer(), destination.AsUnit32(), sendOptions);
    }

private:
    JausAddress myAddress;
    judp2::JUDP judp;
};


int main()
{
    MyClientComponent2 client;

    client.SetAddress(JausAddress("100.2.30"));

    SetEmergency setEmergency;
    setEmergency.EmergencyCode = SetEmergency::STOP;

    client.Send(JausAddress("100.255.10"), setEmergency);

    return 0;
}