#include <stdint.h>
#include <stdexcept>
#include <common/UdpStructs.h>

// This JUDP implementation encapsulates and isolates the JUDP functionality defined
// in AS5669A into a separate class and exposes a simplified interface such that an
// application/protocol that sits on top of JUDP is somewhat prevented from violating
// JUDP protocol. The type of message being sent using JUDP is specified at construction
// time.

namespace judp2
{

enum PriorityEnum { PRIORITY_LOW, PRIORITY_NORMAL, PRIORITY_HIGH, PRIORITY_CRITICAL };

struct SendOptions
{
    enum SendMethodEnum { UNICAST, UNICAST_WITH_ACK, LOCAL_BROADCAST, GLOBAL_BROADCAST };

    bool UseHeaderCompression;
    PriorityEnum Priority;
    SendMethodEnum SendMethod;
};


struct Packet
{
    enum BroadcastEnum { BROADCAST_NONE, BROADCAST_LOCAL, BROADCAST_GLOBAL };
    enum AckNakEnum { ACKNAK_NONE, ACKNAK_REPONSE_REQUIRED, ACK, NAK };
    enum DataFlagsEnum { SINGLE_PACKET, FIRST_PACKET, NORMAL_PACKET, LAST_PACKET };

    unsigned char Version;

    // General Header
    uint8_t MessageType;
    uint8_t HCFlags;
    uint16_t DataSize; // Min: 14
    PriorityEnum Priority;
    BroadcastEnum Broadcast;
    AckNakEnum AckNak;
    DataFlagsEnum DataFlags;
    uint32_t DestinationID;
    uint32_t SourceID;
    char* Payload;
    uint16_t SequenceNumber;

    char* AsByteBuffer();
};


class JUDP
{
public:
    JUDP(uint8_t messageType)
    {
        this->messageType = messageType;
    }

    void setSourceAddress(uint32_t address)
    {
        this->address = address;
    }

    void Send(char* payload, uint32_t destination, SendOptions options)
    {
        judp2::Packet packet;

        // JUDP Packet Header
        packet.Version = 2;

        // General Purpose Header
        packet.MessageType = messageType;
        if (options.UseHeaderCompression)
        {
            configureHeaderCompression(packet);
        }
        calculateDataSize(packet);

        packet.Priority = options.Priority;

        // JUDP separates the destination address from the send method
        // (ie. unicast v global broadcast v local broadcast)
        // That means that JUDP doesn't enforce messages to a specific a destination id needs to be
        // sent a specific way. The application/protocol that sits on top of JUDP would need to enforce
        // this relationship; if necessary.

        switch (options.SendMethod)
        {
            case SendOptions::UNICAST:
                packet.Broadcast = Packet::BROADCAST_NONE;
                packet.AckNak = Packet::ACKNAK_NONE;
                break;

            case SendOptions::UNICAST_WITH_ACK:
                packet.Broadcast = Packet::BROADCAST_NONE;
                packet.AckNak = Packet::ACKNAK_REPONSE_REQUIRED;
                break;

            case SendOptions::GLOBAL_BROADCAST:
                packet.Broadcast = Packet::BROADCAST_GLOBAL;
                packet.AckNak = Packet::ACKNAK_NONE;
                break;

            case SendOptions::LOCAL_BROADCAST:
                packet.Broadcast = Packet::BROADCAST_LOCAL;
                packet.AckNak = Packet::ACKNAK_NONE;
                break;
        }

        packet.DataFlags = judp2::Packet::SINGLE_PACKET;

        packet.SourceID = this->address;
        packet.DestinationID = destination;
        packet.Payload = payload;

        // In this approach, the JUDP implementation manages it's own sequence number.
        packet.SequenceNumber = ++seqNum;

        // To send the message using UDP, we need to lookup the IP address and port
        // that maps to the destination id.
        InetAddress destIp = lookupIpFromJudpAddress(destination);
        // What happens if we don't find an IP:Port for the destination?

        udp::sendto(destIp, packet.AsByteBuffer());
    }

private:
    InetAddress lookupIpFromJudpAddress(uint32_t address);
    void configureHeaderCompression(judp2::Packet packet);
    void calculateDataSize(judp2::Packet packet);

    uint8_t messageType;
    uint32_t address;
    uint16_t seqNum;
};

}