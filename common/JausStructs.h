#include <string>

struct JausAddress
{
    JausAddress();
    JausAddress(std::string);

    uint16_t SubsystemID;
    uint8_t NodeID;
    uint8_t ComponentID;

    uint32_t AsUnit32();
};

struct JausMessage
{
    char* AsByteBuffer();

    uint16_t Id;
};

struct SetEmergency : public JausMessage
{
    static uint16_t ID;

    enum EmergencyCodeEnum { STOP };

    EmergencyCodeEnum EmergencyCode;
};

struct QueryEventTimeout : public JausMessage
{
    static uint16_t ID;
};

struct ReportEventTimeout : public JausMessage
{
    static uint16_t ID;
};