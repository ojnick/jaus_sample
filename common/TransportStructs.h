#include "JausStructs.h"

struct Transport_ReceiveRec
{
    JausAddress SourceID;
    JausMessage MessagePayload;
};

struct Transport_SendRec
{
    bool ReliableDelivery; // REQUIRED
    JausAddress DestinationID; // REQUIRED
    JausAddress SourceID;
    uint8_t Priority; // Enum: LOW, NORMAL, HIGH, CRITICAL
    JausMessage MessagePayload;  // REQUIRED
};
