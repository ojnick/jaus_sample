#include <common/JausStructs.h>
#include <common/TransportStructs.h>

class TransportServiceDefinition;

// All the Protocol Behavior specified by the standard is implemented in here and generated from the JSIDL
// All the Applciation Behavior required by the standard is implemented by a user in a derived class
class EventsServiceDefinition // extends TransportServiceDefinition
{
public:
    void SetTransportServiceDefinition(TransportServiceDefinition* transportService)
    {
        this->transportService = transportService;
    }

    // *** Protocol Behavior ***
    // Assertion: The standard (JSIDL) should allow for the following Protocol Behavior code
    // to be completely generated from the JSIDL without any modification by the final implementer

    // Actions related to Protocol Behavior

    void sendReportEventTimeout(QueryEventTimeout query, Transport_ReceiveRec transportData)
    {
        // In JSIDL: transport.Send('ReportEventTimeout', msg, transportData)

        // NOTE: The way this action is presented in the pdf is very different from the JSIDL definition
        // in that there is no action called sendReportEventTimeout in the JSIDL and this action name is
        // derived from 'extending' the transport.Send action with the first parameter set to 'ReportEventTimeout'

        Transport_SendRec sendRec;

        sendRec.DestinationID = transportData.SourceID;
        // sendRec.ReliableDelivery = ?; // Decision on if this should be sent reliably or not is an implementation decision? - Added to AS5710A, not in AS5710
        // sendRec.SourceID = ?; // Service Definitions are allowed to change/specify the Source JAUS address?
        // sendRec.Priority = ?; // Decision on the priority is an implementation decision?
        sendRec.MessagePayload = ReportEventTimeout();

        transportService->Send(sendRec);
    }

    void sendReportEvents(); // transport.Send('ReportEvents', msg, transportData)
    void sendConfirmEventRequest();
    void sendRejectEventRequest();
    void sendEvent();
    void sendCommandEvent(); // Not defined in JSIDL but could be?

    // Internal Events
    // Service Implementations use these methods to trigger the corresponding transition in the Protocol Behavior
    void EventOccurred();
    void EventError();
    void Timeout();
    void CommandCompleted();
    void CommandExpired();


    // *** Application Behavior ***

    // Actions related to Application Behavior
    // The behavior of these actions are described in the standard but how it is implemented is up the implementer

    virtual void createEvent() = 0;
    virtual void updateEvent() = 0;
    virtual void cancelEvent() = 0;
    virtual void createCommandEvent() = 0;
    virtual void updateCommandEvent() = 0;
    virtual void resetEventTimer() = 0;
    virtual void stopEventTimer() = 0;
    virtual void startCommandTimer() = 0;
    virtual void stopCommandTimer() = 0;

    // Guards /Conditions
    // The behavior of the guards are outside the scope of the standard other than describing what should happen
    // How they happen is up the implementer
    virtual bool isSupported() = 0;
    virtual bool eventExists() = 0;
    virtual bool isCommandSupported() = 0;

private:
    TransportServiceDefinition* transportService;
};


class TransportServiceDefinition
{
public:
    void Send(Transport_SendRec sendRec)
    {
        Enqueue(sendRec);
    }

private:
    virtual void Enqueue(Transport_SendRec) = 0;
};
