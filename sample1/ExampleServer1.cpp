#include <common/ServiceDefinitions.h>
#include "JudpStructs1.h"


// Implement the Application Behavior for the Events Service
class MyEventsServiceImplementation : public EventsServiceDefinition
{
    // Actions related to Application Behavior
    void createEvent() override
    {
        // Create Event and store internally
    }

    void updateEvent() override;
    void cancelEvent() override;
    void resetEventTimer() override;
    void stopEventTimer() override;

    void createCommandEvent() override;
    void updateCommandEvent() override;
    void startCommandTimer() override;
    void stopCommandTimer() override;

    // Guards /Conditions
    bool isSupported() override;
    bool eventExists() override;
    bool isCommandSupported() override;
};

// Implement the Application Behavior for The Transport Service
// This implementation only supports sending/receiving messages using JUDP
class MyJudpTransportServiceImplementation: public TransportServiceDefinition
{
public:
    void SetAddress(JausAddress address)
    {
        myAddress = address;
    }

    // The Enqueue function does the same work as the MyClientComponent1::Send
    // function. The server and client need to ensure that are using the same
    // underlying protocol (such JUDP in this case) to allow message exchange.
    void Enqueue(Transport_SendRec sendRec)
    {
        judp1::Packet packet;

        // JUDP Packet Header
        packet.Version = 2;

        // General Purpose Header
        packet.MessageType = 0;  // 0 = JAUS, 1 - 32 = Reserved -- AS5669A
        packet.HCFlags = 0; // No header compression
        packet.DataSize = 14 + sizeof(sendRec.MessagePayload); // Assuming no HC


        // The Transport Service Implementation can decide to send messages with
        // different priorities and/or with different reliability requirements,
        // based on the message being sent, or by the intended recipient, etc
        // It must still conform to the JUDP protocol however, so some combinations
        // may not be allowed (such as using BROADCAST and ACK/NAK together).
        if (sendRec.MessagePayload.Id == SetEmergency::ID)
        {
            packet.Priority = judp1::Packet::PRIORITY_CRITICAL;
            packet.AckNak = judp1::Packet::ACKNAK_REPONSE_REQUIRED;
        }
        else if (sendRec.DestinationID.ComponentID == 23)
        {
            packet.Priority = judp1::Packet::PRIORITY_LOW;
        }
        else
        {
            packet.Priority = judp1::Packet::PRIORITY_NORMAL;
        }

        // JUDP doesn't specify any relationship between (JUDP) destination id and
        // send method (unicast v local broadcast v global broadcast), while JAUS
        // does have a relatonship between destination JAUS address and send method.
        // That relationship is enforced here.

        if (sendRec.DestinationID.SubsystemID == 65535)
        {
            packet.Broadcast = judp1::Packet::BROADCAST_GLOBAL;
        }
        else if (sendRec.DestinationID.NodeID == 255)
        {
            packet.Broadcast = judp1::Packet::BROADCAST_LOCAL;
        }
        else if (sendRec.DestinationID.ComponentID == 255)
        {
            packet.Broadcast = judp1::Packet::BROADCAST_LOCAL /*?? What to do for Component level broadcast messages? */;
        }
        else
        {
            packet.Broadcast = judp1::Packet::BROADCAST_NONE;
        }


        packet.DataFlags = judp1::Packet::SINGLE_PACKET;

        packet.DestinationID = sendRec.DestinationID.AsUnit32();
        packet.SourceID = myAddress.AsUnit32(); // SendRec allows we to spoof the Source?
        packet.Payload = sendRec.MessagePayload.AsByteBuffer();
        packet.SequenceNumber = ++seqNum;

        judp.Send(packet);
    }

private:
    JausAddress myAddress;
    judp1::JUDP judp;
    uint16_t seqNum;
};

// Put together the Service Implementations under a containing Component
class MyEventsServerComponent
{
public:
    MyEventsServerComponent()
    {
        eventsService.SetTransportServiceDefinition(&transportService);
    }

    void SetAddress(JausAddress address)
    {
        transportService.SetAddress(address);
    }

    int Run();

private:
    MyJudpTransportServiceImplementation transportService;
    MyEventsServiceImplementation eventsService;
};

int main()
{
    MyEventsServerComponent server;

    server.SetAddress(JausAddress("100.10.30"));
    return server.Run();
}

