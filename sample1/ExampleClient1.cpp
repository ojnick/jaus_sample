#include "JudpStructs1.h"
#include <common/JausStructs.h>

// The standard doesn't specify client behavior so the only requirement for a client
// is that is needs to send properly formed JAUS messages using a transport/communication
// protocol that the server understands. In this example, that is JUDP.
// As long as it conforms with the transport/communicationprotocol it is using the client
// can send messages with any options it likes. For JUDP that means a client can use any
// Priority, use Ack/Nak, or maybe even send JAUS unicast messages using JUDP Broadcast?

// This example uses the JudpStructs1 sample code, which is a very light JUDP implementation.
// The ClientComponent (MyClientComponent1) is responsible for ensuring that it does not violate
// the JUDP protocol

class MyClientComponent1
{
public:
    MyClientComponent1() {};

    // The
    void SetAddress(JausAddress address)
    {
        myAddress = address;
    }

    void Send(JausAddress destination, JausMessage message)
    {
        judp1::Packet packet;

        // JUDP Packet Header
        packet.Version = 2;

        // General Purpose Header
        packet.MessageType = 0;  // 0 = JAUS, 1 - 32 = Reserved -- AS5669A
        packet.HCFlags = 0; // No header compression
        packet.DataSize = 14 + sizeof(message); // Assuming no HC


        // The client can decide to send messages with different priorities and/or with
        // different reliability requirements, based on the message being sent, or by the
        // intended recipient, etc
        // It must still conform to the JUDP protocol however, so some combinations
        // may not be allowed (such as using BROADCAST and ACK/NAK together).
        if (message.Id == SetEmergency::ID)
        {
            packet.Priority = judp1::Packet::PRIORITY_CRITICAL;
            packet.AckNak = judp1::Packet::ACKNAK_REPONSE_REQUIRED;
        }
        else if (destination.ComponentID == 23)
        {
            packet.Priority = judp1::Packet::PRIORITY_LOW;
        }
        else
        {
            packet.Priority = judp1::Packet::PRIORITY_NORMAL;
        }

        // JUDP doesn't specify any relationship between (JUDP) destination id and
        // send method (unicast v local broadcast v global broadcast), while JAUS
        // does have a relatonship between destination JAUS address and send method.
        // That relationship is enforced here.

        if (destination.SubsystemID == 65535)
        {
            packet.Broadcast = judp1::Packet::BROADCAST_GLOBAL;
        }
        else if (destination.NodeID == 255)
        {
            packet.Broadcast = judp1::Packet::BROADCAST_LOCAL;
        }
        else if (destination.ComponentID == 255)
        {
            packet.Broadcast = judp1::Packet::BROADCAST_LOCAL /*?? What to do for Component level broadcast messages? */;
        }
        else
        {
            packet.Broadcast = judp1::Packet::BROADCAST_NONE;
        }


        packet.DataFlags = judp1::Packet::SINGLE_PACKET;

        packet.DestinationID = destination.AsUnit32();
        packet.SourceID = myAddress.AsUnit32();
        packet.Payload = message.AsByteBuffer();
        packet.SequenceNumber = ++seqNum;

        // Who is in charge of setting the sequence number?
        // Is the sequence number a per transport protocol number or a per application number?
        // Does it matter?

        judp.Send(packet);
    }

private:
    void calculateDataSize(judp1::Packet packet);

    judp1::JUDP judp;
    JausAddress myAddress;
    uint16_t seqNum;
};


int main()
{
    MyClientComponent1 client;

    client.SetAddress(JausAddress("100.2.30"));

    SetEmergency setEmergency;
    setEmergency.EmergencyCode = SetEmergency::STOP;

    client.Send(JausAddress("100.255.10"), setEmergency);

    return 0;
}