#include <stdint.h>
#include <common/UdpStructs.h>

// This JUDP implementation encapsulates and isolates the JUDP functionality defined
// in AS5669A into a separate class and exposes all JUDP header flags such that an
// applcation/protocol that sits on top of JUDP has the ability to directly control
// the flags used in the header, which could cause it to violate the JUDP protocol.



namespace judp1
{

struct Packet
{
    enum PriorityEnum { PRIORITY_LOW, PRIORITY_NORMAL, PRIORITY_HIGH, PRIORITY_CRITICAL };
    enum BroadcastEnum { BROADCAST_NONE, BROADCAST_LOCAL, BROADCAST_GLOBAL };
    enum AckNakEnum { ACKNAK_NONE, ACKNAK_REPONSE_REQUIRED, ACK, NAK };
    enum DataFlagsEnum { SINGLE_PACKET, FIRST_PACKET, NORMAL_PACKET, LAST_PACKET };

    unsigned char Version;

    // General Header
    uint8_t MessageType;
    uint8_t HCFlags;
    uint16_t DataSize; // Min: 14
    PriorityEnum Priority;
    BroadcastEnum Broadcast;
    AckNakEnum AckNak;
    DataFlagsEnum DataFlags;
    uint32_t DestinationID;
    uint32_t SourceID;
    char* Payload;
    uint16_t SequenceNumber;

    char* AsByteBuffer();
};


class JUDP
{
public:
    void Send(judp1::Packet packet)
    {
        InetAddress destIp = lookupIpFromJudpAddress(packet.DestinationID);
        udp::sendto(destIp, packet.AsByteBuffer());
    }

private:
    InetAddress lookupIpFromJudpAddress(uint32_t address);
};

}